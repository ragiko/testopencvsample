#include <jni.h>
#include <opencv2/opencv.hpp>

extern "C" {
JNIEXPORT void JNICALL Java_com_example_testopencvsample_MainActivity_ImageProcessing(
		JNIEnv* env, jobject thiz, jint width, jint height, jintArray src,
		jintArray dst) {

	jint* _src = env->GetIntArrayElements(src, 0);
	jint* _dst = env->GetIntArrayElements(dst, 0);

	cv::Mat msrc(height, width, CV_8UC4, (unsigned char *) _src);
	cv::Mat mdst(height, width, CV_8UC4, (unsigned char *) _dst);

    cv::cvtColor(msrc, mdst, CV_BGRA2RGBA, 4);

	env->ReleaseIntArrayElements(src, _src, 0);
	env->ReleaseIntArrayElements(dst, _dst, 0);

}
}

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_LIB_TYPE:=STATIC
OPENCV_INSTALL_MODULES:=on

include $(OPENCV_MK_PATH)

LOCAL_MODULE    := TestOpenCVSample
LOCAL_SRC_FILES := TestOpenCVSample.cpp

include $(BUILD_SHARED_LIBRARY)

package com.example.testopencvsample;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {

	ImageView imageView;
	Button button;
	Bitmap bm;
	int buttonPushCnt = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(this);
		imageView = (ImageView) findViewById(R.id.imageView1);

		// assetからimg取得
		try {
			InputStream is = getResources().getAssets().open("satomi.jpeg");
			bm = BitmapFactory.decodeStream(is);
			imageView.setImageBitmap(bm);
		} catch (IOException e) {
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClick(View v) {
		if (v == button) {
			if (buttonPushCnt % 2 == 0) {
				if (imageView != null && bm != null) {

					int src[] = new int[bm.getWidth() * bm.getHeight()];
					int[] dst = new int[bm.getWidth() * bm.getHeight()];
					bm.getPixels(src, 0, bm.getWidth(), 0, 0, bm.getWidth(),
							bm.getHeight());

					// opencvの処理
					ImageProcessing(bm.getWidth(), bm.getHeight(), src, dst);

					Bitmap bm_dst = Bitmap.createBitmap(bm.getWidth(),
							bm.getHeight(), Bitmap.Config.ARGB_8888);
					bm_dst.setPixels(dst, 0/* offset */,
							bm.getWidth() /* stride */, 0, 0, bm.getWidth(),
							bm.getHeight());

					imageView.setImageBitmap(bm_dst);
				}
			} else {
				imageView.setImageBitmap(bm);
			}
			
			buttonPushCnt++;
		}
	}

	public native void ImageProcessing(int width, int height, int src[],
			int[] dst);

	static {
		System.loadLibrary("TestOpenCVSample");
	}

}
